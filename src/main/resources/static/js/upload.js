$( document ).ready(function() {

    $("#uploadButton").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#uploadForm')[0];

    		// Create an FormData object
            var formData = new FormData(form);

    		// disabled the submit button
            $("#uploadButton").prop("disabled", true);

            var bar = $('.bar');
            var percent = $('.percent');

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/api/sessions/upload",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                xhr: function()
                  {
                    var xhr = new window.XMLHttpRequest();
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                        //Do something with upload progress
                        bar.width(percentComplete + '%')
                        percent.html(percentComplete + "%")
                        console.log(percentComplete);
                      }
                    }, false);
                    return xhr;
                  },
                success: function (data) {

                    $("#result").text(data.uuid);
                    console.log("SUCCESS : ", data);
                    $("#uploadButton").prop("disabled", false);

                },
                error: function (e) {

                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    $("#uploadButton").prop("disabled", false);

                }
            });

        });

});