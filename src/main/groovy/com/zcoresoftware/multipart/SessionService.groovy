package com.zcoresoftware.multipart

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service

import java.util.zip.Deflater

@Slf4j
@Service
class SessionService {
    final ThreadLocal<Deflater> deflateStore = new ThreadLocal<Deflater>() {
        @Override
        protected Deflater initialValue() {
            Deflater deflater = new Deflater()
            return deflater
        }
    };

    Session upload(UUID sessionUuid, Long fragmentSize, InputStream inputStream) {
        Session session = null

        if (inputStream != null) {
            byte[] buffer = new byte[fragmentSize]

            int bytesRead
            int fragments = 0

            while ((bytesRead = inputStream.read(buffer)) > 0) {
                byte[] uncompressed = new byte[bytesRead]

                System.arraycopy(buffer, 0, uncompressed, 0, bytesRead)

                byte[] compressed = compress(uncompressed)

                log.info("{} Uncompressed: {} Compressed size: {}", (fragments + 1), uncompressed.length, compressed.length)

                fragments++
            }
            session = new Session(uuid: sessionUuid)
        }

        return session
    }

    byte[] compress(byte[] uncompressed) {
        byte[] deflateBuffer = new byte[uncompressed.length]
        byte[] output

        int bytesOut = 0

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(uncompressed.length)

        Deflater deflater = deflateStore.get()

        deflater.setInput(uncompressed)

        deflater.finish()

        while (!deflater.finished()) {
            int count = deflater.deflate(deflateBuffer)
            bytesOut += count
            log.info("count: {} ", count)
            byteArrayOutputStream.write(deflateBuffer, 0, count)
        }

        byteArrayOutputStream.close()
        output = new byte[bytesOut]

        System.arraycopy(byteArrayOutputStream.toByteArray(), 0, output, 0, bytesOut)

        deflater.reset()

        return output

    }
}
