package com.zcoresoftware.multipart

import groovy.util.logging.Slf4j
import org.apache.commons.fileupload.FileItemIterator
import org.apache.commons.fileupload.FileItemStream
import org.apache.commons.fileupload.servlet.ServletFileUpload
import org.apache.commons.fileupload.util.Streams
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpServletRequest

@Slf4j
@RestController
@RequestMapping(value="/api/sessions")
class SessionController {
    @Autowired
    SessionService sessionService

    @RequestMapping(method = RequestMethod.GET)
    Session retrieveSession() {
        return new Session(uuid: UUID.randomUUID(), name: "THe Name")
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    Session upload(HttpServletRequest httpServletRequest) {
        final ServletFileUpload upload = new ServletFileUpload()
        final FileItemIterator iterator = upload.getItemIterator(httpServletRequest)

        InputStream is = null

        Long chunkSize = 10240

        Session session = null

        while (iterator.hasNext()) {
            final FileItemStream item = iterator.next()

            if (!item.isFormField()) {
                log.info("Found file to upload")
                is = item.openStream()

                session = sessionService.upload(UUID.randomUUID(), chunkSize, is)

                System.out.flush()
                is.close()
            } else {
                String fieldName = item.getFieldName();

                String value = Streams.asString(item.openStream())

                switch (fieldName) {
                    case "chunkSize" :
                        chunkSize = Long.parseLong(value)
                        break
                    default:
                        log.error("Unrecognized parameter")
                }

                log.info("Got a parameter")
            }
        }



        return session
    }
}
